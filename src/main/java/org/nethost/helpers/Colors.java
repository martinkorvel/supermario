package org.nethost.helpers;

import java.awt.*;

public class Colors {
    public static Color transparent() {
        return new Color(0,0,0, 0);
    }

    public static Color marioRed() {
        return new Color(216, 40,0,255);
    }

    public static Color marioHair() {
        return new Color(135, 112,0,255);
    }

    public static Color marioSkin() {
        return new Color(252, 152, 56,255);
    }

    public static Color marioSky() {
        return new Color(103, 135, 246);
    }
}
