package org.nethost.helpers;

public class Vector2 {
    public Vector2() {

    }

    public Vector2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    private int x,y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
