package org.nethost.gameObjects;

import org.nethost.engine.object.GameObject;
import org.nethost.sprites.BaseSprite;
import org.nethost.sprites.ISprite;

import java.util.HashMap;

public class Goomba extends GameObject {
    public int currentSprite = 26;

    public Goomba () {
        HashMap<Integer, HashMap<Integer, ISprite>> sprites = new HashMap<>();

        HashMap<Integer, ISprite> column = new HashMap<>();

        column.put(0, new BaseSprite(currentSprite));

        sprites.put(0,column);

        this.setSprites(sprites);
    }

    public void animate() {
        if(currentSprite == 26) {
            currentSprite = 27;
        } else if (currentSprite == 27){
            currentSprite = 28;
        } else {
            currentSprite = 26;
        }

        HashMap<Integer, HashMap<Integer, ISprite>> sprites = new HashMap<>();

        HashMap<Integer, ISprite> column = new HashMap<>();

        column.put(0, new BaseSprite(currentSprite));

        sprites.put(0,column);

        this.setSprites(sprites);
    }
}
