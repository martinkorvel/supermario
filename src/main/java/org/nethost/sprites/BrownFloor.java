package org.nethost.sprites;

import org.nethost.engine.sprite.Sprite;
import org.nethost.engine.sprite.SpriteEngine;

public class BrownFloor extends SpriteEngine implements ISprite{
    public int spriteIndex = 52;

    public Sprite getSprite(int x, int y) {
        return super.getSprite(spriteIndex, x, y);
    }
}
