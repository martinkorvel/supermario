package org.nethost.sprites;

import org.nethost.engine.sprite.Sprite;

public interface ISprite {
    Sprite getSprite(int x, int y);
    Sprite getSprite(int index, int x, int y);
}
