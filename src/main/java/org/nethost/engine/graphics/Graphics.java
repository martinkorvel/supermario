package org.nethost.engine.graphics;

import org.nethost.FirstScene;
import org.nethost.engine.EngineGlobals;
import org.nethost.engine.Settings;
import org.nethost.gameObjects.BigBush;
import org.nethost.gameObjects.BrownFloorBrick;
import org.nethost.helpers.Colors;
import org.nethost.helpers.Vector2;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Graphics {
    public static void init() {
        java.awt.Graphics bbg = EngineGlobals.backBuffer.getGraphics();

        bbg.setColor(Colors.marioSky());
        bbg.fillRect(0, 0, Settings.windowWidth, Settings.windowHeight);

        FirstScene.goomba1.setSolid(true);
        FirstScene.goomba1.setKinematic(true);
        FirstScene.goomba1.setPosition(new Vector2(200, 192));

        FirstScene.floor1.setSolid(true);
        FirstScene.floor1.setPosition(new Vector2(-48,208));

        FirstScene.floor2.setSolid(true);
        FirstScene.floor2.setPosition(new Vector2(144,208));

        FirstScene.bush.setSolid(false);
        FirstScene.bush.setPosition(new Vector2(0, 160));
    }

    public static void draw() {
        java.awt.Graphics g = EngineGlobals.mainGUI.getGraphics();
        java.awt.Graphics bbg = EngineGlobals.backBuffer.getGraphics();
        bbg.setColor(Colors.marioSky());
        bbg.fillRect(0, 0, Settings.windowWidth, Settings.windowHeight);

        FirstScene.bush.draw();
        FirstScene.floor1.draw();
        FirstScene.floor2.draw();

        if(FirstScene.goomba1.getPosition().getX() <= 0) {
            FirstScene.goomba1.setPosition(new Vector2(200, FirstScene.goomba1.getPosition().getY()));
        }

        FirstScene.goomba1.setPosition(new Vector2(FirstScene.goomba1.getPosition().getX() - 1, FirstScene.goomba1.getPosition().getY()));
        FirstScene.goomba1.draw();

        g.drawImage(EngineGlobals.backBuffer, EngineGlobals.insets.left, EngineGlobals.insets.top, EngineGlobals.mainGUI);

        if(FirstScene.animationCounter >= 10) {
            Graphics.handleAnimation();
            FirstScene.animationCounter = 0;
        }

        FirstScene.animationCounter++;

        Graphics.handleGoombaCollider();
    }

    public static void handleGoombaCollider() {
        if(FirstScene.goomba1.getPosition().getX()<=128) {
            FirstScene.goomba1.setPosition(new Vector2(FirstScene.goomba1.getPosition().getX(), FirstScene.goomba1.getPosition().getY()+2));
        }

        if(FirstScene.goomba1.getPosition().getY() > 500) {
            FirstScene.goomba1.setPosition(new Vector2(200,192));
        }
    }

    public static void handleAnimation() {
        FirstScene.goomba1.animate();
    }

    public static BufferedImage scale(BufferedImage imageToScale, int dWidth, int dHeight) {
        BufferedImage scaledImage = null;
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.dispose();
        }
        return scaledImage;
    }
}
