package org.nethost;

import org.nethost.gameObjects.BigBush;
import org.nethost.gameObjects.BrownFloorBrick;
import org.nethost.gameObjects.Goomba;

public class FirstScene {
    public static Goomba goomba1 = new Goomba();

    public static BrownFloorBrick floor1 = new BrownFloorBrick();
    public static BrownFloorBrick floor2 = new BrownFloorBrick();
    public static BigBush bush = new BigBush();

    public static int animationCounter = 0;
}
